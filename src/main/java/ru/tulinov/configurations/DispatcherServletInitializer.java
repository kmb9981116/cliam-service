package ru.tulinov.configurations;

import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

public class DispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Filter[] getServletFilters() {
        Filter securityFilter = new DelegatingFilterProxy("springSecurityFilterChain");
        return new Filter[] { securityFilter };
    }
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {SpringConfig.class, SecurityConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[0];
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }

}