package ru.tulinov.configurations;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    private static final String TOPIC_NAME = "notification-topic";

    @Bean
    public NewTopic notificationTopic() {
        return TopicBuilder.name(TOPIC_NAME)
                .build();
    }
}
