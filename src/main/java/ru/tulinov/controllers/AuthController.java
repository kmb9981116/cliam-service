package ru.tulinov.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.tulinov.dtos.AuthenticationRequestDTO;
import ru.tulinov.dtos.TokenDTO;
import ru.tulinov.servicies.AuthService;

@RestController
@Slf4j
@RequestMapping(value = "/api/v1/auth/login")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public TokenDTO login(@RequestBody AuthenticationRequestDTO requestDto) {
        return authService.login(requestDto);
    }

}
