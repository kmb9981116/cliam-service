package ru.tulinov.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.tulinov.dtos.ClaimDTO;
import ru.tulinov.servicies.ClaimService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/claims")
@Slf4j
public class ClaimController {

    private final ClaimService claimService;

    @Autowired
    public ClaimController(ClaimService claimService) {
        this.claimService = claimService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClaimDTO createClaim(@RequestBody ClaimDTO claimDTO) {
        return claimService.createClaim(claimDTO);
    }

    @GetMapping(path = "/{number}")
    @ResponseStatus(HttpStatus.OK)
    public String getClaimStatusByNumber(@PathVariable String number) {
        return claimService.getClaimStatusByNumber(number);
    }

    @GetMapping(path = "/passportNumber/{passportNumber}")
    @ResponseStatus(HttpStatus.OK)
    public List<ClaimDTO> getAllClaimsByPassportNumber(@PathVariable String passportNumber) {
        return claimService.getAllClaimsByPassportNumber(passportNumber);
    }

    @GetMapping(path = "/login/{login}")
    @ResponseStatus(HttpStatus.OK)
    public List<ClaimDTO> getAllClaimsByLogin(@PathVariable String login) {
        return claimService.getAllClaimsByLogin(login);
    }
}