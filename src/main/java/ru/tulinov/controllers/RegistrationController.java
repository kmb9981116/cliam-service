package ru.tulinov.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.tulinov.dtos.EmployeeDTO;
import ru.tulinov.servicies.EmployeeService;

@RestController
@Slf4j
@RequestMapping(value = "/api/v1/auth/register")
public class RegistrationController {

    private final EmployeeService employeeService;

    @Autowired
    public RegistrationController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeDTO registerEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return employeeService.registrationEmployee(employeeDTO);
    }
}
