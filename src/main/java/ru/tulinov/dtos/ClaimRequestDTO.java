package ru.tulinov.dtos;

import lombok.*;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Component
public class ClaimRequestDTO {
    private String number;
    private String claimType;
    private String departmentCode;
    private String claimStatus;
    private String passportNumber;
    private String authorFullName;
}
