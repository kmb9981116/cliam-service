package ru.tulinov.dtos;

import lombok.*;
import org.springframework.stereotype.Component;
import ru.tulinov.models.Role;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Component
public class EmployeeDTO {
    private String login;
    private String password;
    private String employeeFullName;
    private List<Role> roles;
}
