package ru.tulinov.mappers;

import ru.tulinov.dtos.ClaimDTO;
import ru.tulinov.models.Claim;

public class ClaimMapper {
    public static ClaimDTO mapToDto(Claim claim) {
        return ClaimDTO.builder()
                .number(claim.getNumber())
                .claimType(claim.getTypeId())
                .departmentCode(claim.getDepartmentId())
                .claimStatus(claim.getStatusId())
                .passportNumber(claim.getPassportNumber())
                .authorFullName(claim.getAuthorFullName())
                .employeeLogin(claim.getEmployeeId())
                .build();
    }

    public static Claim mapToClaim(ClaimDTO claimDTO) {
        return Claim.builder()
                .number(claimDTO.getNumber())
                .typeId(claimDTO.getClaimType())
                .departmentId(claimDTO.getDepartmentCode())
                .statusId(claimDTO.getClaimStatus())
                .passportNumber(claimDTO.getPassportNumber())
                .authorFullName(claimDTO.getAuthorFullName())
                .employeeId(claimDTO.getEmployeeLogin())
                .build();
    }
}
