package ru.tulinov.mappers;

import ru.tulinov.dtos.ClaimDTO;
import ru.tulinov.dtos.ClaimRequestDTO;

public class ClaimRequestMapper {

    public static ClaimRequestDTO mapClaimDtoToClaimRequestDto(ClaimDTO claimDTO) {
        return ClaimRequestDTO.builder()
                .number(claimDTO.getNumber())
                .claimType(claimDTO.getClaimType())
                .departmentCode(claimDTO.getDepartmentCode())
                .claimStatus(claimDTO.getClaimStatus())
                .passportNumber(claimDTO.getPassportNumber())
                .authorFullName(claimDTO.getAuthorFullName())
                .build();
    }
}
