package ru.tulinov.mappers;

import ru.tulinov.dtos.EmployeeDTO;
import ru.tulinov.models.Employee;

public class EmployeeMapper {
    public static EmployeeDTO mapToDto(Employee employee) {
        return EmployeeDTO.builder()
                .login(employee.getLogin())
                .password(employee.getPassword())
                .employeeFullName(employee.getFullname())
                .build();
    }

    public static Employee mapToEmployee(EmployeeDTO employeeDTO) {
        return Employee.builder()
                .login(employeeDTO.getLogin())
                .password(employeeDTO.getPassword())
                .fullname(employeeDTO.getEmployeeFullName())
                .build();
    }
}
