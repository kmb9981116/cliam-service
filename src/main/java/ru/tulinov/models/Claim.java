package ru.tulinov.models;

import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "t_claims")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Claim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "number", nullable = false, unique = true)
    private String number;
    @Column(name = "type_id", nullable = false, length = 50)
    private String typeId;
    @Column(name = "department_id", nullable = false, length = 50)
    private String departmentId;
    @Column(name = "status_id", nullable = false, length = 50)
    private String statusId;
    @Column(name = "passport_number", nullable = false, length = 50)
    private String passportNumber;
    @Column(name = "author_full_name", nullable = false)
    private String authorFullName;
    @Column(name = "employee_id", nullable = false, length = 50)
    private String employeeId;

}