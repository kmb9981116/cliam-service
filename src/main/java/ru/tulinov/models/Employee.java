package ru.tulinov.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_employees")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Employee  {
    @Id
    @Column(length = 50)
    private String login;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String fullname;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "t_employee_roles",
    joinColumns = {@JoinColumn(name = "employee_login", referencedColumnName = "login")},
    inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

}
