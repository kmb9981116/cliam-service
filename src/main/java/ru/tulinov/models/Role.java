package ru.tulinov.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_roles")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
    private List<Employee> employees;


}
