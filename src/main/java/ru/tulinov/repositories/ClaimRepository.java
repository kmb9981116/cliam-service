package ru.tulinov.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tulinov.models.Claim;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClaimRepository extends JpaRepository<Claim, Long> {
    Optional<Claim> findClaimByNumber(String number);
    boolean existsClaimByPassportNumber(String passportNumber);
    List<Claim> findAllClaimsByPassportNumber(String passportNumber);
    boolean existsClaimByEmployeeId(String login);
    List<Claim> findAllClaimsByEmployeeId(String login);
}