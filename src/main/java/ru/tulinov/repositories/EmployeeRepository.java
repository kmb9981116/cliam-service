package ru.tulinov.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tulinov.models.Employee;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, String> {
    Optional<Employee> findEmployeeByLogin(String login);
}
