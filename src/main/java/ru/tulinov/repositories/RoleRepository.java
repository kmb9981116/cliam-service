package ru.tulinov.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tulinov.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
