package ru.tulinov.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.tulinov.models.Employee;
import ru.tulinov.servicies.EmployeeService;

@Slf4j
@Service
public class JwtUserDetailsService implements UserDetailsService {
    private final EmployeeService employeeService;

    @Autowired
    public JwtUserDetailsService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Employee employee = employeeService.findEmployeeByLogin(login);
        if (employee == null) {
            throw new UsernameNotFoundException("Сотрудник с login: " + login + " не найден");
        }
        return JwtUserFactory.create(employee);
    }
}
