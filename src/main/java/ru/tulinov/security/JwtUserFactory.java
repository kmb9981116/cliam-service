package ru.tulinov.security;

import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.tulinov.models.Employee;
import ru.tulinov.models.Role;

import java.util.List;
import java.util.stream.Collectors;
@NoArgsConstructor
public final class JwtUserFactory {
    public static JwtUser create(Employee employee) {
        return new JwtUser(
                employee.getLogin(),
                employee.getPassword(),
                mapToGrantedAuthorities(employee.getRoles()));
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
    }
}
