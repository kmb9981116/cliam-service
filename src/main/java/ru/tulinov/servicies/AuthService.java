package ru.tulinov.servicies;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.tulinov.dtos.AuthenticationRequestDTO;
import ru.tulinov.dtos.TokenDTO;
import ru.tulinov.models.Employee;
import ru.tulinov.security.JwtTokenProvider;

@Service
@Slf4j
public class AuthService {

    private final EmployeeService employeeService;
    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public AuthService(EmployeeService employeeService,
                       AuthenticationManager authenticationManager,
                       JwtTokenProvider jwtTokenProvider) {
        this.employeeService = employeeService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    /**
     * Документация метода login
     *
     * @param requestDto DTO содержащее необходимую информацию для аутентификации.
     * @return DTO содержащее authentication token и login работника.
     * @throws BadCredentialsException Если пароль или логин не верные.
     */

    public TokenDTO login(AuthenticationRequestDTO requestDto) {
        log.info("Аутентификация пользователя");
        try {
            String login = requestDto.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login,
                    requestDto.getPassword()));
            Employee employee = employeeService.findEmployeeByLogin(login);
            if (employee == null) {
                throw new UsernameNotFoundException("Пользователь не найден");
            }
            String token = jwtTokenProvider.createToken(login, employee.getRoles());
            return new TokenDTO(login, token);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Неправильный пароль или login");
        }
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<String> handleBadCredentialsException(BadCredentialsException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }
}
