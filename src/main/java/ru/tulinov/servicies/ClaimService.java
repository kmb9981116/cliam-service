package ru.tulinov.servicies;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import ru.tulinov.dtos.ClaimDTO;
import ru.tulinov.dtos.ClaimRequestDTO;
import ru.tulinov.dtos.ClaimResponseDTO;
import ru.tulinov.enums.Status;
import ru.tulinov.event.ClaimCreatedEvent;
import ru.tulinov.mappers.ClaimMapper;
import ru.tulinov.mappers.ClaimRequestMapper;
import ru.tulinov.models.Claim;
import ru.tulinov.models.Employee;
import ru.tulinov.repositories.ClaimRepository;
import ru.tulinov.repositories.EmployeeRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ClaimService {

    private final KafkaTemplate<String, ClaimCreatedEvent> kafkaTemplate;
    private final ClaimRepository claimRepository;
    private final EmployeeRepository employeeRepository;
    private final WebClient.Builder webClientBuilder;
    private static final String INVENTORY_URI = "http://localhost:7776/api/v1/claims";

    @Autowired
    public ClaimService(ClaimRepository claimRepository, EmployeeRepository employeeRepository,
                        KafkaTemplate<String, ClaimCreatedEvent> kafkaTemplate,
                        WebClient.Builder webClientBuilder) {
        this.claimRepository = claimRepository;
        this.employeeRepository = employeeRepository;
        this.kafkaTemplate = kafkaTemplate;
        this.webClientBuilder = webClientBuilder;
    }

    public ClaimDTO createClaim(ClaimDTO claimDTO) {
        log.info("Создание Заявления");
        validateClaim(claimDTO);
        Claim claim = ClaimMapper.mapToClaim(claimDTO);
        claim.setStatusId(Status.NEW.getValue());
        log.info("Отпрака уведомления на создание заявки с номером {}", claim.getNumber());
        kafkaTemplate.send("notification-topic", new ClaimCreatedEvent(claim.getNumber()));
        log.info("Заявка с номером = {} сохранена", claim.getNumber());
        claimRepository.save(claim);
        log.info("Вызов interdepartmental Interaction Service");
        ClaimRequestDTO claimRequest = ClaimRequestMapper.mapClaimDtoToClaimRequestDto(claimDTO);
        String response = webClientBuilder.build().post()
                .uri(INVENTORY_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(claimRequest))
                .retrieve()
                .bodyToMono(String.class)
                .block();
        claim.setStatusId(Status.PROCESSING.getValue());
        claimRepository.save(claim);
        log.info("Статус заявления обновлен");
        log.info(response);
        return ClaimMapper.mapToDto(claim);
    }

    public String getClaimStatusByNumber(String number) {
        log.trace("Проверка существования заявления с number = {}", number);
        Optional<Claim> claim = claimRepository.findClaimByNumber(number);
        if (claim.isEmpty()) {
            log.warn("Заявление с number = {} не найдено", number);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        String claimStatus = claim.get().getStatusId();
        if (claimStatus.equals(Status.PROCESSING.getValue())) {
            log.info("Вызов interdepartmental Interaction Service");
            ClaimResponseDTO responseStatus = webClientBuilder.build().get()
                    .uri(INVENTORY_URI,
                            uriBuilder -> uriBuilder.queryParam("number", number).build())
                    .retrieve()
                    .bodyToMono(ClaimResponseDTO.class)
                    .block();

            String newRespStatus = responseStatus.getClaimStatus();
            if (newRespStatus.equals(Status.COMPLETED.getValue())) {
                log.info("Обновление статсута заявления");
                claim.get().setStatusId(Status.COMPLETED.getValue());
                claimRepository.save(claim.get());
                return claim.get().getStatusId();
            }
        }
        log.info("Заявление со статусом = {} получено", claimStatus);
        return claimStatus;
    }

    public List<ClaimDTO> getAllClaimsByPassportNumber(String passportNumber) {
        log.trace("Проверка существования заявлений от ФЗ = {}", passportNumber);
        if (!claimRepository.existsClaimByPassportNumber(passportNumber)) {
            log.warn("Заявлений от ФЗ с passportNumber = {} не найдено", passportNumber);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        List<Claim> claims = claimRepository.findAllClaimsByPassportNumber(passportNumber);
        log.info("Заявления ФЗ = {} получены", passportNumber);
        return claims.stream().map(ClaimMapper::mapToDto).collect(Collectors.toList());
    }

    public List<ClaimDTO> getAllClaimsByLogin(String login) {
        log.trace("Проверка существования заявлений от сотрудника = {}", login);
        if (!claimRepository.existsClaimByEmployeeId(login)) {
            log.warn("Заявлений от сотрудника с login = {} не найдено", login);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        List<Claim> claims = claimRepository.findAllClaimsByEmployeeId(login);
        log.info("Заявления сотрудника = {} получены", login);
        return claims.stream().map(ClaimMapper::mapToDto).collect(Collectors.toList());
    }

    private void validateClaim(ClaimDTO claimDTO) {
        log.trace("Проверка существования заявления с number = {}", claimDTO.getNumber());
        Optional<Claim> savedClaim = claimRepository.findClaimByNumber(claimDTO.getNumber());
        if (savedClaim.isPresent()) {
            log.warn("Заявление с number = {} уже существует", claimDTO.getNumber());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Заявление с таким номером уже существует");
        }
        log.trace("Проверка существования работника с login = {}", claimDTO.getEmployeeLogin());
        Optional<Employee> employee = employeeRepository.findEmployeeByLogin(claimDTO.getEmployeeLogin());
        if (employee.isEmpty()) {
            log.warn("Работника с login = {} не существует", claimDTO.getEmployeeLogin());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Работника с таким login не существует");
        }
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<String> handleResponseStatusException(ResponseStatusException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

}
