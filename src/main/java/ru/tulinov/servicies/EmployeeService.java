package ru.tulinov.servicies;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;
import ru.tulinov.dtos.EmployeeDTO;
import ru.tulinov.mappers.EmployeeMapper;
import ru.tulinov.models.Employee;
import ru.tulinov.models.Role;
import ru.tulinov.repositories.EmployeeRepository;
import ru.tulinov.repositories.RoleRepository;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EmployeeService  {
    private final EmployeeRepository employeeRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository,
                           RoleRepository roleRepository,
                           BCryptPasswordEncoder encoder) {
        this.employeeRepository = employeeRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
    }

    /**
     * Документация метода registerEmployee
     *
     * @param employeeDTO содержит необходимую информацию для регистрации нового работника: (логин, пароль, ФИО).
     * По умолчанию, только что зарегистрированные пользователи получают роль ROLE_USER.
     * @return DTO с информацией зарегистрированного пользователя
     */

    public EmployeeDTO registrationEmployee(EmployeeDTO employeeDTO) {
        log.info("Регистрация нового пользователя");
        validateEmployee(employeeDTO);
        Employee employee = EmployeeMapper.mapToEmployee(employeeDTO);
        Role userRole = roleRepository.findByName("ROLE_USER");
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(userRole);

        employee.setPassword(encoder.encode(employeeDTO.getPassword()));
        employee.setRoles(userRoles);

        Employee registredEmployee = employeeRepository.save(employee);
        log.info("Успешно зарегистрирован пользователь {}", registredEmployee);
        return EmployeeMapper.mapToDto(registredEmployee);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public Employee findEmployeeByLogin(String login) {
        Optional<Employee> employee = employeeRepository.findEmployeeByLogin(login);
        log.trace("Проверка существования пользователя");
        if (employee.isEmpty()) {
            log.warn("Пользователь не найден");
            throw new EntityNotFoundException("Пользователь не найден");
        }
        Employee findedemployee = employee.get();
        log.info("Найден пользователь {} по логину {}", findedemployee, login);

        Hibernate.initialize(findedemployee.getRoles());
        return findedemployee;
    }

    private void validateEmployee(EmployeeDTO employeeDTO) {
        log.trace("Проверка существования работника в базе с login = {}", employeeDTO.getLogin());
        Optional<Employee> savedEmployee = employeeRepository.findEmployeeByLogin(employeeDTO.getLogin());
        if (savedEmployee.isPresent()) {
            log.warn("Работник с login = {} уже зарегистрирован", employeeDTO.getLogin());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Работник с таким login уже был зарегистрирован");
        }
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<String> handleResponseStatusException(ResponseStatusException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
}