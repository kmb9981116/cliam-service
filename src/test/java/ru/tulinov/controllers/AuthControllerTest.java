//package ru.tulinov.controllers;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.http.MediaType;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import ru.tulinov.dtos.AuthenticationRequestDTO;
//import ru.tulinov.models.Employee;
//import ru.tulinov.security.JwtTokenProvider;
//import ru.tulinov.servicies.EmployeeService;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@ExtendWith(MockitoExtension.class)
//class AuthControllerTest {
//
//    @Mock
//    private EmployeeService employeeService;
//
//    @Mock
//    private AuthenticationManager authenticationManager;
//
//    @Mock
//    private JwtTokenProvider jwtTokenProvider;
//
//    @InjectMocks
//    private AuthController authController;
//
//    private MockMvc mockMvc;
//    private ObjectMapper objectMapper;
//
//    @BeforeEach
//    void setUp() {
//        mockMvc = MockMvcBuilders.standaloneSetup(authController).build();
//        objectMapper = new ObjectMapper();
//    }
//
//    @Test
//    void successfulAuthentication() throws Exception {
//        String login = "testUser";
//        String password = "testPassword";
//        String token = "token";
//        AuthenticationRequestDTO requestDTO = new AuthenticationRequestDTO(login, password);
//        String requestJson = objectMapper.writeValueAsString(requestDTO);
//        Employee employee = Employee.builder()
//                .login(login)
//                .password(password)
//                .fullname("Test User")
//                .build();
//
//        Mockito.when(employeeService.findEmployeeByLogin(login)).thenReturn(employee);
//        Mockito.when(jwtTokenProvider.createToken(login, employee.getRoles())).thenReturn(token);
//
//        mockMvc.perform(post("/api/v1/auth/login")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(requestJson))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.login").value(login))
//                .andExpect(jsonPath("$.token").value(token));
//    }
//
//    @Test
//    void failedAuthenticationInvalidPassword() throws Exception {
//        String login = "testUser";
//        String password = "BadPassword";
//        AuthenticationRequestDTO requestDTO = new AuthenticationRequestDTO(login, password);
//        String requestJson = objectMapper.writeValueAsString(requestDTO);
//        Employee employee = Employee.builder()
//                .login(login)
//                .password(password)
//                .fullname("Test User")
//                .build();
//
//        Mockito.when(employeeService.findEmployeeByLogin(login)).thenReturn(employee);
//        Mockito.doThrow(new BadCredentialsException("Неправильный пароль или login"))
//                .when(jwtTokenProvider)
//                .createToken(login, employee.getRoles());
//
//        mockMvc.perform(post("/api/v1/auth/login")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(requestJson))
//                        .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    void failedAuthenticationInvalidLogin() throws Exception {
//        String login = "nonExistingUser";
//        String password = "testPassword";
//        AuthenticationRequestDTO requestDTO = new AuthenticationRequestDTO(login, password);
//        String requestJson = objectMapper.writeValueAsString(requestDTO);
//
//        Mockito.when(employeeService.findEmployeeByLogin(login)).thenReturn(null);
//
//        mockMvc.perform(post("/api/v1/auth/login")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(requestJson))
//                        .andExpect(status().isUnauthorized());
//    }
//
//}