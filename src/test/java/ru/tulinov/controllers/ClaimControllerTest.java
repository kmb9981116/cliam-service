package ru.tulinov.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.server.ResponseStatusException;
import ru.tulinov.dtos.ClaimDTO;
import ru.tulinov.models.Claim;
import ru.tulinov.repositories.ClaimRepository;
import ru.tulinov.repositories.EmployeeRepository;
import ru.tulinov.servicies.ClaimService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class ClaimControllerTest {

    @Mock
    private ClaimService claimService;

    @InjectMocks
    private ClaimController claimController;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;
    private final EmployeeRepository employeeRepository = Mockito.mock(EmployeeRepository.class);
    private final ClaimRepository claimRepository = Mockito.mock(ClaimRepository.class);

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(claimController).build();
        objectMapper = new ObjectMapper();
    }


    @Test
    void createClaimSuccessful() throws Exception {
        ClaimDTO claimDTO = ClaimDTO.builder()
                .number("54321")
                .claimType("DOC-PFR")
                .departmentCode("DEP-PFR")
                .claimStatus("NEW")
                .passportNumber("123321")
                .authorFullName("artur")
                .employeeLogin("login")
                .build();
        String requestJson = objectMapper.writeValueAsString(claimDTO);
        ClaimDTO createdClaimDTO = ClaimDTO.builder()
                .number("54321")
                .claimType("DOC-PFR")
                .departmentCode("DEP-PFR")
                .claimStatus("NEW")
                .passportNumber("123321")
                .authorFullName("artur")
                .employeeLogin("login")
                .build();

        Mockito.when(claimService.createClaim(Mockito.any(ClaimDTO.class)))
                .thenReturn(createdClaimDTO);

        mockMvc.perform(post("/api/v1/claims")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated());
    }


    @Test
    void createClaimBadRequestNumberAlreadyExists() throws Exception {
        ClaimDTO claimDTO = ClaimDTO.builder()
                .number("54321")
                .claimType("DOC-PFR")
                .departmentCode("DEP-PFR")
                .claimStatus("NEW")
                .passportNumber("123321")
                .authorFullName("artur")
                .employeeLogin("login")
                .build();
        String requestJson = objectMapper.writeValueAsString(claimDTO);
        Claim existingClaim = new Claim();


        Mockito.when(claimRepository.findClaimByNumber(claimDTO.getNumber()))
                .thenReturn(Optional.of(existingClaim));
        Mockito.when(claimService.createClaim(Mockito.any(ClaimDTO.class)))
                .thenThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST));

        mockMvc.perform(post("/api/v1/claims")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    void createClaimBadRequestEmployeeLoginNotFound() throws Exception {
        ClaimDTO claimDTO = ClaimDTO.builder()
                .number("54321")
                .claimType("DOC-PFR")
                .departmentCode("DEP-PFR")
                .claimStatus("NEW")
                .passportNumber("123321")
                .authorFullName("artur")
                .employeeLogin("login")
                .build();
        String requestJson = objectMapper.writeValueAsString(claimDTO);

        Mockito.when(employeeRepository.findEmployeeByLogin(claimDTO.getEmployeeLogin()))
                .thenReturn(Optional.empty());
        Mockito.when(claimService.createClaim(Mockito.any(ClaimDTO.class)))
                .thenThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST));

        mockMvc.perform(post("/api/v1/claims")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    /**
     * Документация метода getClaimStatusByNumberSuccessful
     *
     * @param expectedStatus В параметрах содержится ожидаемый статус.
     * Данный тест параметризированный, потому что потом количество статусов может увеличится.
     *
     */

    @ParameterizedTest
    @ValueSource(strings = { "NEW", "COMPLETED", "PROCESSING" })
    @DisplayName("Тест getClaimStatusByNumber с разными статусами")
    void getClaimStatusByNumberSuccessful(String expectedStatus) throws Exception {
        String claimNumber = "123";

        Mockito.when(claimService.getClaimStatusByNumber(claimNumber)).thenReturn(expectedStatus);

        mockMvc.perform(get("/api/v1/claims/{number}", claimNumber))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedStatus));
    }

    @Test
    void getClaimStatusByNumberNoClaimsFound() throws Exception {
        String number = "123";

        Mockito.when(claimService.getClaimStatusByNumber(number))
                .thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND));

        mockMvc.perform(get("/api/v1/claims/{number}", number)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void getAllClaimsByPassportNumberSuccessful() throws Exception {
        String passportNumber = "123456789";
        List<ClaimDTO> claims = Arrays.asList(
                ClaimDTO.builder()
                        .number("1").claimType("DOC-PFR").claimStatus("PROCESSING")
                        .departmentCode("DEP-PFR").passportNumber(passportNumber).authorFullName("smb")
                        .employeeLogin("admin").build(),
                ClaimDTO.builder().number("2").claimType("DOC-FNS").claimStatus("PROCESSING")
                        .departmentCode("DEP-FNS").passportNumber(passportNumber).authorFullName("smb")
                        .employeeLogin("admin").build(),
                ClaimDTO.builder().number("3").claimType("DOC-FNS").claimStatus("PROCESSING")
                        .departmentCode("DEP-FNS").passportNumber(passportNumber).authorFullName("smb")
                        .employeeLogin("admin").build()
        );


        Mockito.when(claimService.getAllClaimsByPassportNumber(passportNumber)).thenReturn(claims);

        mockMvc.perform(get("/api/v1/claims/passportNumber/{passportNumber}", passportNumber)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].number").value("1"))
                .andExpect(jsonPath("$[0].claimType").value("DOC-PFR"))
                .andExpect(jsonPath("$[0].claimStatus").value("PROCESSING"))
                .andExpect(jsonPath("$[0].departmentCode").value("DEP-PFR"))
                .andExpect(jsonPath("$[0].passportNumber").value(passportNumber))
                .andExpect(jsonPath("$[0].authorFullName").value("smb"))
                .andExpect(jsonPath("$[0].employeeLogin").value("admin"))
                .andExpect(jsonPath("$[1].number").value("2"))
                .andExpect(jsonPath("$[1].claimType").value("DOC-FNS"))
                .andExpect(jsonPath("$[1].claimStatus").value("PROCESSING"))
                .andExpect(jsonPath("$[1].departmentCode").value("DEP-FNS"))
                .andExpect(jsonPath("$[1].passportNumber").value(passportNumber))
                .andExpect(jsonPath("$[1].authorFullName").value("smb"))
                .andExpect(jsonPath("$[1].employeeLogin").value("admin"))
                .andExpect(jsonPath("$[2].number").value("3"))
                .andExpect(jsonPath("$[2].claimType").value("DOC-FNS"))
                .andExpect(jsonPath("$[2].claimStatus").value("PROCESSING"))
                .andExpect(jsonPath("$[2].departmentCode").value("DEP-FNS"))
                .andExpect(jsonPath("$[2].passportNumber").value(passportNumber))
                .andExpect(jsonPath("$[2].authorFullName").value("smb"))
                .andExpect(jsonPath("$[2].employeeLogin").value("admin"));
    }

    @Test
    void getAllClaimsByPassportNumberNoClaimsFound() throws Exception {
        String passportNumber = "123456789";

        Mockito.when(claimService.getAllClaimsByPassportNumber(passportNumber))
                .thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND));

        mockMvc.perform(get("/api/v1/claims/passportNumber/{passportNumber}", passportNumber)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void getAllClaimsByLoginSuccessful() throws Exception {
        String login = "admin";
        List<ClaimDTO> claims = Arrays.asList(
                ClaimDTO.builder()
                        .number("1").claimType("DOC-PFR").claimStatus("PROCESSING")
                        .departmentCode("DEP-PFR").passportNumber("123456789").authorFullName("smb")
                        .employeeLogin(login).build(),
                ClaimDTO.builder().number("2").claimType("DOC-FNS").claimStatus("PROCESSING")
                        .departmentCode("DEP-FNS").passportNumber("123456789").authorFullName("smb")
                        .employeeLogin(login).build(),
                ClaimDTO.builder().number("3").claimType("DOC-FNS").claimStatus("PROCESSING")
                        .departmentCode("DEP-FNS").passportNumber("123456789").authorFullName("smb")
                        .employeeLogin(login).build()
        );


        Mockito.when(claimService.getAllClaimsByLogin(login)).thenReturn(claims);

        mockMvc.perform(get("/api/v1/claims/login/{login}", login)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].number").value("1"))
                .andExpect(jsonPath("$[0].claimType").value("DOC-PFR"))
                .andExpect(jsonPath("$[0].claimStatus").value("PROCESSING"))
                .andExpect(jsonPath("$[0].departmentCode").value("DEP-PFR"))
                .andExpect(jsonPath("$[0].passportNumber").value("123456789"))
                .andExpect(jsonPath("$[0].authorFullName").value("smb"))
                .andExpect(jsonPath("$[0].employeeLogin").value("admin"))
                .andExpect(jsonPath("$[1].number").value("2"))
                .andExpect(jsonPath("$[1].claimType").value("DOC-FNS"))
                .andExpect(jsonPath("$[1].claimStatus").value("PROCESSING"))
                .andExpect(jsonPath("$[1].departmentCode").value("DEP-FNS"))
                .andExpect(jsonPath("$[1].passportNumber").value("123456789"))
                .andExpect(jsonPath("$[1].authorFullName").value("smb"))
                .andExpect(jsonPath("$[1].employeeLogin").value("admin"))
                .andExpect(jsonPath("$[2].number").value("3"))
                .andExpect(jsonPath("$[2].claimType").value("DOC-FNS"))
                .andExpect(jsonPath("$[2].claimStatus").value("PROCESSING"))
                .andExpect(jsonPath("$[2].departmentCode").value("DEP-FNS"))
                .andExpect(jsonPath("$[2].passportNumber").value("123456789"))
                .andExpect(jsonPath("$[2].authorFullName").value("smb"))
                .andExpect(jsonPath("$[2].employeeLogin").value("admin"));
    }

    @Test
    void getAllClaimsByLoginNoClaimsFound() throws Exception {
        String login = "login";

        Mockito.when(claimService.getAllClaimsByLogin(login))
                .thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND));

        mockMvc.perform(get("/api/v1/claims/login/{login}", login)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}