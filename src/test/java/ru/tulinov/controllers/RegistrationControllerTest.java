package ru.tulinov.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.server.ResponseStatusException;
import ru.tulinov.dtos.EmployeeDTO;
import ru.tulinov.models.Role;
import ru.tulinov.servicies.EmployeeService;

import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class RegistrationControllerTest {

    @Mock
    private EmployeeService employeeService;
    @InjectMocks
    private RegistrationController registrationController;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(registrationController).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void registerEmployeeSuccessful() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .login("login")
                .password("test")
                .employeeFullName("fullName")
                .build();
        String requestJson = objectMapper.writeValueAsString(employeeDTO);
        EmployeeDTO registratedemployeeDTO = EmployeeDTO.builder()
                .login("login")
                .password("test")
                .employeeFullName("fullName")
                .roles(Collections.singletonList(Role.builder().name("ROLE_USER").build()))
                .build();

        Mockito.when(employeeService.registrationEmployee(Mockito.any(EmployeeDTO.class)))
                .thenReturn(registratedemployeeDTO);


        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.login").value("login"))
                .andExpect(jsonPath("$.employeeFullName").value("fullName"))
                .andExpect(jsonPath("$.password").value("test"));

    }

    @Test
    void registerEmployeeWithExistingLogin() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .login("existingLogin")
                .password("test")
                .employeeFullName("fullName")
                .build();
        String requestJson = objectMapper.writeValueAsString(employeeDTO);

        Mockito.when(employeeService.registrationEmployee(Mockito.any(EmployeeDTO.class)))
                .thenThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

}